﻿using System;
using System.Drawing;

namespace Tetrominos.Objects
{
    public class Tetromino
    {
        private Brush _brush;
        public BlockColors BlockColor;
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public Int32[,] Shape { get; set; }

        //{0, 0, 1},
        //{1, 1, 1}

        public void Draw(Graphics g)
        {
            if (_brush == null) {
                _brush = new SolidBrush(BoardState.GetBlockColor(BlockColor));
            }

            for (var row = X; row < X + Shape.GetLength(0); row++) {
                for (var col = Y; col < Y + Shape.GetLength(1); col++) {
                    if (Shape[row - X, col - Y] != 0) {
                        g.FillRectangle(_brush, row * Width, col * Width, Width, Width);
                    }
                }
            }
        }

        public void Move(Direction direction)
        {
            if (direction == Direction.Right && BoardState.AttemptMove(this, X + 1, Y)) {
                X += 1;
            } else if (direction == Direction.Left && BoardState.AttemptMove(this, X - 1, Y)) {
                X -= 1;
            }
        }

        public void Drop(DropSpeed drop)
        {
            if (drop == DropSpeed.Soft && BoardState.AttemptMove(this, X, Y + 1)) {
                Y += 1;
            } else if (drop == DropSpeed.Hard) {
                while (BoardState.AttemptMove(this, X, Y + 1)) {
                    Y += 1;
                }
            }
        }

        public void Rotate()
        {
            var n = Shape.GetLength(0);
            var col = Shape.GetLength(1);
            var newShape = new int[col, n];

            for (var i = 0; i < n; ++i) {
                for (var j = 0; j < n; ++j) {
                    newShape[i, j] = Shape[n - j - 1, i];
                }
            }

            Shape = newShape;
        }
    }
}
