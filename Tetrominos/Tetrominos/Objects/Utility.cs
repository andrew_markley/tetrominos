﻿namespace Tetrominos.Objects
{
    public enum Direction
    {
        Right = 1,
        Left = -1
    }

    public enum DropSpeed
    {
        Hard = 1,
        Soft = 2
    }

    public enum BlockColors
    {
        Red = 1,
        Yellow = 2,
        Green = 3,
        Blue = 4,
        Orange = 5,
        Purple = 6,
        Cyan = 7
    }
}
