﻿using System;
using System.Drawing;

namespace Tetrominos.Objects
{
    public class BoardState
    {
        public static Int32[,] Board;
        public Int32 BlockWidth;
        public Tetromino CurrentTetromino;

        public BoardState(int boardWidth, int boardHeight, int blockWidth)
        {
            Board = new Int32[boardWidth, boardHeight];
            BlockWidth = blockWidth;

            CurrentTetromino = new Tetromino
            {
                BlockColor = BlockColors.Red,
                Width = 25,
                X = 2,
                Y = 2,
                Shape = new[,]
                {
                    {1, 0},
                    {1, 0},
                    {1, 1}
                }
            };
        }

        public static bool AttemptMove(Tetromino currentTetromino, int futureX, int futureY)
        {
            if (currentTetromino.Shape.GetLength(0) + futureX > Board.GetLength(0)) {
                return false;
            }

            if (currentTetromino.Shape.GetLength(1) + futureY > Board.GetLength(1)) {
                return false;
            }

            var tempBoard = new Int32[Board.GetLength(0), Board.GetLength(1)];
            for (var row = futureX; row < currentTetromino.Shape.GetLength(0); row++) {
                for (var col = futureY; col < currentTetromino.Shape.GetLength(1); col++) {
                    if (currentTetromino.Shape[row - futureX, col - futureY] != 0) {
                        tempBoard[row, col] = (int) currentTetromino.BlockColor;
                        return false;
                    }
                }
            }

            for (var row = 0; row < Board.GetLength(0); row++) {
                for (var col = 0; col < Board.GetLength(1); col++) {
                    if (Board[row, col] != 0 && tempBoard[row, col] != 0) {
                        return false;
                    }
                }
            }

            return true;
        }

        public void DrawBoard(Graphics g)
        {
            for (var row = 0; row < Board.GetLength(0); row++) {
                for (var col = 0; col < Board.GetLength(1); col++) {
                    var block = Board[row, col];
                    Brush brush = new SolidBrush(GetBlockColor((BlockColors) block));
                    g.FillRectangle(brush, row * BlockWidth, col * BlockWidth, BlockWidth, BlockWidth);

                    //if (CurrentTetromino.X >= row && CurrentTetromino.X + CurrentTetromino.Shape.GetLength(0) <= row) {
                    //    if (CurrentTetromino.Y >= col && CurrentTetromino.Y + CurrentTetromino.Shape.GetLength(1) <= col) {
                    //        brush = new SolidBrush(GetBlockColor(CurrentTetromino.BlockColor));
                    //        g.FillRectangle(brush, row * BlockWidth, col * BlockWidth, BlockWidth, BlockWidth);
                    //    }
                    //}
                }
            }

            CurrentTetromino.Draw(g);
        }

        public static Color GetBlockColor(BlockColors color)
        {
            switch (color) {
                case BlockColors.Red:
                    return Color.Red;

                case BlockColors.Yellow:
                    return Color.Yellow;

                case BlockColors.Purple:
                    return Color.Purple;

                case BlockColors.Orange:
                    return Color.Orange;

                case BlockColors.Green:
                    return Color.Green;

                case BlockColors.Cyan:
                    return Color.Cyan;

                case BlockColors.Blue:
                    return Color.Blue;

                default:
                    return Color.White;
            }
        }
    }
}
