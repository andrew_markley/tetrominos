﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Tetrominos.Objects;

namespace Tetrominos
{
    public partial class Form1 : Form
    {
        private Graphics _graphics;
        public BoardState BoardState;
        
        public Form1()
        {
            InitializeComponent();

            _graphics = gamePanel.CreateGraphics();
            BoardState = new BoardState(gamePanel.Width / 25, gamePanel.Height / 25, 25);

            var timer = new Timer { Interval = 350 };
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            BoardState.CurrentTetromino.Drop(DropSpeed.Soft);
            BoardState.DrawBoard(_graphics);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyValue) {
                case (int)Keys.Left: BoardState.CurrentTetromino.Move(Direction.Left);
                    break;
                case (int)Keys.Right: BoardState.CurrentTetromino.Move(Direction.Right);
                    break;
                case (int)Keys.Space: BoardState.CurrentTetromino.Drop(DropSpeed.Hard);
                    break;
                case (int)Keys.Up: BoardState.CurrentTetromino.Rotate();
                    break;
                case (int)Keys.Down: BoardState.CurrentTetromino.Drop(DropSpeed.Soft);
                    break;
            }
        }

        private void gamePanel_Paint(object sender, PaintEventArgs e)
        {
            BoardState.DrawBoard(_graphics);
        }
    }
}
